/**
 * @file logconfig.cpp
 * Implementation file for class CLogConfig
 *
 * @author João Silva <jazzy.blog@gmail.com>
 *
 * @date September 1, 2019
 *
 * @par Copyright:
 * 
 */

#include "logconfig.h"

using namespace pocoHttpServer::config;

CLogConfig::CLogConfig() :
m_logType(OT_Console)
{
}

void CLogConfig::setLogType(OutputType logType)
{
	m_logType = logType;
	if(m_logType==OT_Console && m_LogFileConfig)
	{
		// Resets log file configuration
		m_LogFileConfig = boost::none;
	}
}

CLogConfig::CLogConfig(const CLogConfig& orig)
{
}

CLogConfig::~CLogConfig()
{
}

void CLogConfig::addLogModule(std::string sModule, Poco::Message::Priority prio)
{
	m_mDebugLevel.insert(
			std::pair<std::string, Poco::Message::Priority>(sModule, prio));
}

CLogConfig::DebugLevelIter CLogConfig::DebugLevelBegin()
{
	return m_mDebugLevel.begin();
}

CLogConfig::DebugLevelIter CLogConfig::DebugLevelEnd()
{
	return m_mDebugLevel.end();
}

CLogConfig::OutputType CLogConfig::getLogType() const
{
	return m_logType;
}

boost::optional<CLogConfig::FileLogOutputConfig> CLogConfig::getFileLog() const
{
	return m_LogFileConfig;
}

void CLogConfig::setFileLog(std::string sFilename, std::string sRotation)
{
	m_LogFileConfig = FileLogOutputConfig(sFilename, sRotation);
}
