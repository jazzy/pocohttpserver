/**
 * @file typedefinitions.h
 * Declaration file
 *
 * @author João Silva <jazzy.blog@gmail.com>
 *
 * @date July 5, 2015
 *
 * @par Copyright:
 * 
 */

#ifndef TYPEDEFINITIONS_H
#define TYPEDEFINITIONS_H

#include <boost/property_tree/ptree.hpp>

namespace simulationengine
{
	namespace config
	{
		typedef unsigned long timestamp_t;
		typedef boost::property_tree::ptree simobjdata_t;
	}
}

#endif /* TYPEDEFINITIONS_H */

