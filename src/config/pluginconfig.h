/*
 * There should be a license here...
 */

/**
 * @file pluginconfig.h
 * Declaration file for class PluginConfig
 *
 * @author Joao Silva <jazzy.blog@gmail.com>
 *
 * @date 16 de Setembro de 2019
 *
 * @par Copyright:
 * 
 */

#ifndef PLUGINCONFIG_H
#define PLUGINCONFIG_H

#include <boost/shared_ptr.hpp>
#include <boost/property_tree/ptree.hpp>
#include <string>
#include <vector>

namespace pocoHttpServer
{
	namespace config
	{

		class PluginConfig
		{
		public:

			class PluginData
			{
			public:
				typedef boost::shared_ptr<PluginData> pointer;

				PluginData() = delete;
				PluginData(std::string uri,
						std::string filename);
				PluginData(std::string uri,
						std::string filename,
						boost::property_tree::ptree config);
				PluginData(const PluginData& orig);
				virtual ~PluginData();

				void pluginFilename(std::string _pluginFilename);
				std::string pluginFilename() const;
				void uri(std::string _uri);
				std::string uri() const;
                boost::property_tree::ptree getConfig() const;
			private:
				std::string _uri;
				std::string _pluginFilename;
				boost::property_tree::ptree _config;
			};

			typedef boost::shared_ptr<PluginConfig> pointer;
			typedef std::vector<PluginData::pointer>::const_iterator PluginData_iterator;


			PluginConfig();
			PluginConfig(const PluginConfig& orig);
			virtual ~PluginConfig();

			void pluginDir(std::string _sPluginDir);
			std::string pluginDir() const;
			void loadDefaultHandlers(bool _bLoadDefaultHandlers);
			bool loadDefaultHandlers() const;

			size_t getNrOfConfiguredPlugins() const
			{
				return _plugins.size();
			};

			PluginData_iterator getPluginsDataBegin() const
			{
				return _plugins.begin();
			};

			PluginData_iterator getPluginsDataEnd() const
			{
				return _plugins.end();
			};

			PluginData_iterator addPluginConfig(PluginData::pointer pPlugin);

		private:
			bool _bLoadDefaultHandlers;
			std::string _sPluginDir;
			std::vector<PluginData::pointer> _plugins;
		};

	}
}

#endif /* PLUGINCONFIG_H */

