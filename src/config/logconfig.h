/**
 * @file logconfig.h
 * Declaration file for class CLogConfig
 *
 * @author João Silva <jazzy.blog@gmail.com>
 *
 * @date September 1, 2019
 *
 * @par Copyright:
 * 
 */

#ifndef LOGCONFIG_H
#define LOGCONFIG_H

#include <map>
#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/optional.hpp>
#include <Poco/Message.h>

namespace pocoHttpServer
{
	namespace config
	{

		class CLogConfig
		{
		public:

			typedef enum
			{
				OT_Console,
				OT_File
			} OutputType;

			class FileLogOutputConfig
			{
			public:

				explicit FileLogOutputConfig(std::string sFilename, std::string sRotation) :
				m_sFilename(sFilename),
				m_sRotation(sRotation)
				{
				};

				FileLogOutputConfig(const FileLogOutputConfig& orig) :
				m_sFilename(orig.m_sFilename),
				m_sRotation(orig.m_sRotation)
				{
				};

				virtual ~FileLogOutputConfig()
				{
				}

                std::string getRotation() const
                {
                	return m_sRotation;
                }

                std::string getFilename() const
                {
                	return m_sFilename;
                };
			private:
				std::string m_sFilename;
				std::string m_sRotation;
			};

			typedef boost::shared_ptr<CLogConfig> pointer;
			typedef std::map<std::string, Poco::Message::Priority>::iterator DebugLevelIter;

			CLogConfig();
			CLogConfig(const CLogConfig& orig);
			virtual ~CLogConfig();

			void addLogModule(std::string sModule, Poco::Message::Priority prio);

			DebugLevelIter DebugLevelBegin();
			DebugLevelIter DebugLevelEnd();

			void setLogType(OutputType logType);

			OutputType getLogType() const;
			void setFileLog(std::string sFilename, std::string sRotation);
			boost::optional<FileLogOutputConfig> getFileLog() const;

		private:
			std::map<std::string, Poco::Message::Priority> m_mDebugLevel;

			OutputType m_logType;
			boost::optional<FileLogOutputConfig> m_LogFileConfig;
		};
	}
}
#endif /* LOGCONFIG_H */

