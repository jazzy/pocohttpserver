/*
 * There should be a license here...
 */

/**
 * @file requesthandler.cpp
 * Declaration file for class RequestHandler
 *
 * @author Joao Silva <jazzy.blog@gmail.com>
 *
 * @date 18 de Fevereiro de 2019
 *
 * @par Copyright:
 * 
 */

#include "requesthandler.h"

#include <sstream>
#include <istream>
#include <iostream>

using namespace pocoHttpServer::requesthandler;

RequestHandler::RequestHandler(MyRequestHandlerFactory::Ptr pFactory) :
_pFactory(pFactory),
IRequestHandler("Default")
{
}

RequestHandler::RequestHandler(const RequestHandler& orig) :
_pFactory(orig._pFactory),
IRequestHandler("Default")
{
}

RequestHandler::~RequestHandler()
{
}

void RequestHandler::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
//	printRequestData(request);
	std::ostream& ostr = response.send();
	ostr <<
			"<html><head><title>HTTP Server powered by POCO C++ Libraries</title></head>";
	ostr << "<body>";
	ostr << "Client address: " << request.getHost() << "<br/>";
	ostr << "Request :" << request.getURI() << "<br/>";
	ostr << link("Status", "status") << "<br/>";
	ostr << "<br>" << link("Exit", "exit") << "<br/>";
	ostr << "</body></html>";
	if (request.getURI() == "/exit")
	{
		_pFactory->stop();
	}
}

std::string RequestHandler::link(std::string sTitle, std::string sURL) const
{
	std::ostringstream ostr;
	ostr << "<a href=""" << sURL << """>";
	ostr << sTitle;
	ostr << "</a>";

	return ostr.str();
}

void RequestHandler::configure(const boost::property_tree::ptree& config)
{

}

void RequestHandler::initialize()
{

}

std::string RequestHandler::name() const
{
	return "Default";
}

using namespace std;

void RequestHandler::printRequestData(Poco::Net::HTTPServerRequest& request) const
{
	cout << "\n\nReceived " << request.getMethod()
			<< " request from " << request.getHost()
			<< " at URI " << request.getURI()
			<< endl;

	istream &is = request.stream();
	string sReqData(std::istreambuf_iterator<char>(is),{});
	//	cout << "Request data: \n" << sReqData << endl;
}

RequestHandler* RequestHandler::clone() const
{
	return new RequestHandler(*this);
}
