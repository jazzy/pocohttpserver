/*
 * There should be a license here...
 */

/**
 * @file requesthandler.h
 * Declaration file for class RequestHandler
 *
 * @author Joao Silva <jazzy.blog@gmail.com>
 *
 * @date 18 de Fevereiro de 2019
 *
 * @par Copyright:
 * 
 */

#ifndef REQUESTHANDLER_H
#define REQUESTHANDLER_H

#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>

#include "myrequesthandlerfactory.h"

namespace pocoHttpServer
{
	namespace requesthandler
	{

		class RequestHandler : public IRequestHandler
		{
		public:
			RequestHandler() = delete;
			RequestHandler(MyRequestHandlerFactory::Ptr pFactory);
			RequestHandler(const RequestHandler& orig);
			virtual ~RequestHandler();

			void configure(const boost::property_tree::ptree& config) override;
			void initialize() override;

			std::string name() const override;

			void handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response) override;

			RequestHandler* clone() const override;
			
		private:
			MyRequestHandlerFactory::Ptr  _pFactory;

			std::string link(std::string sTitle, std::string sURL) const;
			void printRequestData(Poco::Net::HTTPServerRequest& request) const;
		};
	}
}
#endif /* REQUESTHANDLER_H */

