/**
 * @file requesthandlerstore.cpp
 * Implementation file for class CRequestHandlerStore
 *
 * @author João Silva <jazzy.blog@gmail.com>
 *
 * @date September 1, 2019
 *
 * @par Copyright:
 * 
 */

#include <iostream>
#include <Poco/URI.h>

using namespace std;

#include "requesthandlerstore.h"

#include "src/requesthandler/irequesthandler.h"
#include "utils/loggingmanager.h"

using namespace pocoHttpServer;
using namespace pocoHttpServer::requesthandler;

CRequestHandlerStore::Ptr CRequestHandlerStore::getNewStore()
{
	Poco::LogStream logStream(utils::CLoggingManager::getLoggingManager()->getLogger("plugin"));
	logStream.debug() << "Getting plugin store reference" << std::endl;

	static CRequestHandlerStore::Ptr pStore(new CRequestHandlerStore());
	pStore->_init();
	return pStore;
}

void CRequestHandlerStore::_init()
{

}

CRequestHandlerStore::CRequestHandlerStore() : utils::Loggable("plugin")
{
	_logStream.debug() << "Creating plugin store" << std::endl;
}

CRequestHandlerStore::~CRequestHandlerStore()
{
	while (!_requestHandlers.empty())
	{
		IRequestHandler* pReqHandler = _requestHandlers.begin()->second;
		if (pReqHandler)
		{
			delete pReqHandler;
		}
		_requestHandlers.erase(_requestHandlers.begin());
	}
}

IRequestHandler* CRequestHandlerStore::loadRequestHandlerPlugin(std::string sLibraryFile, std::string sReqHandlerId)
{
	IRequestHandler* pRes = NULL;

	PluginLoader loader;
	try
	{
		loader.loadLibrary(sLibraryFile);
	}
	catch (Poco::LibraryLoadException ex)
	{
		_logStream.error()
				<< "Exception:" << ex.displayText()
				<< std::endl;
		return pRes;
	}

	PluginLoader::Iterator it(loader.begin());
	PluginLoader::Iterator end(loader.end());
	for (; it != end; ++it)
	{
		PluginManifest::Iterator itMan(it->second->begin());
		PluginManifest::Iterator endMan(it->second->end());
		for (; itMan != endMan; ++itMan)
		{
			pRes = loader.create(itMan->name());
			if (!pRes)
			{
				_logStream.error()
						<< "Failed to create request handler " << itMan->name()
						<< std::endl;
				//				throw exception::UnableToGetRequestHandlerDriverException(sLibraryFile, itMan->name());
			}
			else
			{
				_logStream.information()
						<< "Adding request handler for URI "
						<< sReqHandlerId
						<< std::endl;
				_requestHandlers[sReqHandlerId] = pRes;
			}
		}
	}

	return pRes;
}

IRequestHandler* CRequestHandlerStore::getRequestHandler(std::string sReqHandlerId)
{
	sReqHandlerId = Poco::URI(sReqHandlerId).getPath();
	_logStream.debug() << "Getting handler " << sReqHandlerId << std::endl;
	
	if (_requestHandlers.find(sReqHandlerId) == _requestHandlers.end())
	{
		_logStream.warning() << "No handler registered for " << sReqHandlerId << std::endl;
		return NULL;
	}
	return _requestHandlers[sReqHandlerId]->clone();
}

IRequestHandler* CRequestHandlerStore::loadEmbeddedRequestHandler(IRequestHandler* pReqHandler, std::string sReqHandlerId)
{
	if (pReqHandler && !sReqHandlerId.empty())
	{
		_requestHandlers[sReqHandlerId] = pReqHandler;
		return _requestHandlers[sReqHandlerId];
	}
	return NULL;
}
