/*
 * There should be a license here...
 */

/**
 * @file statusrequesthandler.cpp
 * Declaration file for class StatusRequestHandler
 *
 * @author Joao Silva <jazzy.blog@gmail.com>
 *
 * @date 19 de Fevereiro de 2019
 *
 * @par Copyright:
 * 
 */

#include "statusrequesthandler.h"
#include <sstream>
#include <istream>
#include <iostream>

using namespace pocoHttpServer::requesthandler;

StatusRequestHandler::StatusRequestHandler(MyRequestHandlerFactory::Ptr pFactory) :
_pFactory(pFactory),
		IRequestHandler("Status")
{
	
}

StatusRequestHandler::StatusRequestHandler(const StatusRequestHandler& orig) :
_pFactory(orig._pFactory),
IRequestHandler("Status")
{
}

StatusRequestHandler::~StatusRequestHandler()
{
}

void StatusRequestHandler::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
//	printRequestData(request);
	std::ostream& ostr = response.send();
	ostr <<
			"<html><head><title>HTTP Server powered by POCO C++ Libraries</title></head>";
	ostr << "<body>";
	ostr << "<h1>Server status</h1>";
	ostr << "Handled requests: " << _pFactory->getRequestsHandled() << "<br/>";
	ostr << link("Home", "..") << "<br/>";
	ostr << "</body></html>";
}

std::string StatusRequestHandler::link(std::string sTitle, std::string sURL) const
{
	std::ostringstream ostr;
	ostr << "<a href=""" << sURL << """>";
	ostr << sTitle;
	ostr << "</a>";
	
	return ostr.str();
}

void StatusRequestHandler::configure(const boost::property_tree::ptree& config)
{

}

void StatusRequestHandler::initialize()
{

}

std::string StatusRequestHandler::name() const
{
	return "status";
}

using namespace std;

void StatusRequestHandler::printRequestData(Poco::Net::HTTPServerRequest& request) const
{
	cout << "\n\nReceived " << request.getMethod() << " request from " << request.getHost() << endl;

	istream &is = request.stream();
	string sReqData(std::istreambuf_iterator<char>(is), {});
//	cout << "Request data: " << sReqData << endl;
}

IRequestHandler* StatusRequestHandler::clone() const
{
	return new StatusRequestHandler(*this);
}
