/*
 * There should be a license here...
 */

/**
 * @file logrequest.cpp
 * Implementation file for class LogRequest
 *
 * @author Joao Silva <jazzy.blog@gmail.com>
 *
 * @date 2019/09/28
 *
 * @par Copyright:
 * 
 */

#include "logrequest.h"

using namespace pocoHttpServer::requesthandler;

LogRequest::LogRequest() : IRequestHandler("LogRequest")
{
}

LogRequest::LogRequest(const LogRequest& orig) : IRequestHandler("LogRequest")
{
}

LogRequest::~LogRequest()
{
}

std::string LogRequest::name() const
{
	return "LogRequest";
}

void LogRequest::configure(const boost::property_tree::ptree& config)
{
	_logStream.information()
			<< "Configuring LogRequest request handler - nothing to configure"
			<< std::endl;
}

void LogRequest::initialize()
{
	_logStream.information()
			<< "Initializing LogRequest request handler - nothing to initialize"
			<< std::endl;
}

void LogRequest::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
	_logStream.information()
			<< "LogRequest request handler - "
			<< request.getMethod()
			<< " for " << request.getURI()
			<< std::endl;
}

IRequestHandler* LogRequest::clone() const
{
	return new LogRequest(*this);
}
