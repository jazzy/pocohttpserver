/*
 * There should be a license here...
 */

/**
 * @file logrequest.h
 * Declaration file for LogRequest class
 *
 * @author João Silva <jazzy.blog@gmail.com>
 *
 * @date 2019/09/28
 *
 * @par Copyright:
 * 
 */

#ifndef LOGREQUEST_H
#define LOGREQUEST_H

#include <Poco/ClassLibrary.h>
#include "requesthandler/irequesthandler.h"

namespace pocoHttpServer
{
	namespace requesthandler
	{

		class LogRequest : public IRequestHandler
		{
		public:
			LogRequest();
			LogRequest(const LogRequest& orig);
			virtual ~LogRequest();
			
			std::string name() const override;
			void configure(const boost::property_tree::ptree& config) override;
			void initialize() override;
			void handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response) override;

			IRequestHandler* clone() const override;

		private:

		};
	}
}


POCO_BEGIN_MANIFEST(pocoHttpServer::requesthandler::IRequestHandler)
	POCO_EXPORT_CLASS(pocoHttpServer::requesthandler::LogRequest)
POCO_END_MANIFEST

#endif /* LOGREQUEST_H */

