# Plugins
## Plugins automatically built
Plugin in the folder logrequest.

### Plugin file
The plugin generates the file logrequest.so, which is the dynamic library that 
implements the plugin's functionality.

### Functional description
This plugin just logs (in the server log file) every request it receives.

## Additional plugins
The information in this chapter is optional and is provided as a guideline.

Additional plugins should be implemented in their own dir under `plugins`.
If it is intended that it is build along with the HTTP server, its dir must be
referenced in the `Makefile.am` file in the plugins dir.
