/*
 * There should be a license here...
 */

/* 
 * File:   invalidconfigfile.h
 * Author: Joao Silva <jazzy.blog@gmail.com>
 *
 * Created on 10 de Setembro de 2019, 17:39
 */

#ifndef INVALIDCONFIGFILE_H
#define INVALIDCONFIGFILE_H

#include "genericexception.h"

namespace pocoHttpServer
{
	namespace exception
	{

		class InvalidConfigFile : public GenericException
		{
		public:
			InvalidConfigFile() = delete;
			InvalidConfigFile(std::string sFile) : _sFile(sFile) {};
			InvalidConfigFile(const InvalidConfigFile& orig) : _sFile(orig._sFile) {};
			virtual ~InvalidConfigFile() {};
			
			std::string what() const override
			{
				return "Could not parse configuration file: " + _sFile;
			};

		private:
			std::string _sFile;
		};

	}
}

#endif /* INVALIDCONFIGFILE_H */

