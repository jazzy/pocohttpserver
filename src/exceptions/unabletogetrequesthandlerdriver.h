/**
 * @file unabletogetrequesthandlerdriver.h
 * @brief 
 *
 *  @author João Silva <jazzy.blog@gmail.com>
 *  @date	September 1, 2019
 */

#ifndef UNABLETOGETREQUESTHANDLERDRIVER_H
#define UNABLETOGETREQUESTHANDLERDRIVER_H


#include "dynlibexception.h"

namespace pocoHttpServer
{
	namespace exception
	{

		class UnableToGetRequestHandlerDriverException : public DynamicLibraryException
		{
		public:

			UnableToGetRequestHandlerDriverException(std::string sLibraryFile,
					std::string sDriver) :
			DynamicLibraryException(sLibraryFile),
			m_sDriver(sDriver) { };

			UnableToGetRequestHandlerDriverException(const UnableToGetSimulatorDriverException& orig) :
			DynamicLibraryException(orig.libraryFile()) { };

			virtual ~UnableToGetRequestHandlerDriverException() { };

			virtual std::string what() const
			{
				return "Unable to get simulator driver: " + m_sDriver + ". Library: "
						+ libraryFile();
			}

		private:
			std::string m_sDriver;
		};
	}
}

#endif /* UNABLETOGETREQUESTHANDLERDRIVER_H */

