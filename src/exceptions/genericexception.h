/**
 * @file genericexception.h
 * Declaration file
 *
 * @author João Silva <jazzy.blog@gmail.com>
 *
 * @date September 1, 2019
 *
 * @par Copyright:
 * 
 */

#ifndef GENERICEXCEPTION_H
#define GENERICEXCEPTION_H

#include <exception>

namespace pocoHttpServer
{
	namespace exception
	{

		class GenericException
		{
		public:
			virtual std::string what() const = 0;
		};
	}
}


#endif /* GENERICEXCEPTION_H */

