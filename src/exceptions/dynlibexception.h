/**
 * @file dynlibexception.h
 * @brief 
 *
 *  @author		@author João Silva <jazzy.blog@gmail.com>
 *  @date		September 1, 2019
 */

#ifndef DYNLIBEXCEPTION_H
#define DYNLIBEXCEPTION_H

#include "genericexception.h"

namespace pocoHttpServer
{
	namespace exception
	{

		class DynamicLibraryException : public GenericException
		{
		public:

			DynamicLibraryException(std::string sLibraryFile) :
			m_sLibraryFile(sLibraryFile) { };

			DynamicLibraryException(const DynamicLibraryException& orig) { };

			virtual ~DynamicLibraryException() { };

			std::string libraryFile() const
			{
				return m_sLibraryFile;
			}

			virtual std::string what() const
			{
				return "Dynamic library exception: " + m_sLibraryFile;
			}

		private:
			std::string m_sLibraryFile;
		};
	}
}


#endif /* DYNLIBEXCEPTION_H */

