/*
 * There should be a license here...
 */

/**
 * @file httpserver.h
 * Declaration file for class httpServer
 *
 * @author Joao Silva <jazzy.blog@gmail.com>
 *
 * @date 3 de Setembro de 2019
 *
 * @par Copyright:
 * 
 */

#ifndef HTTPSERVER_H
#define HTTPSERVER_H

#include <Poco/Net/HTTPServer.h>
#include <boost/noncopyable.hpp>

#include "requesthandler/irequesthandler.h"
#include "requesthandler/myrequesthandlerfactory.h"
#include "requesthandler/requesthandlerstore.h"
#include "config/pluginconfig.h"

namespace pocoHttpServer
{

	class httpServer : public boost::noncopyable, public utils::Loggable
	{
	public:
		httpServer() = delete;
		httpServer(requesthandler::MyRequestHandlerFactory::Ptr pFactory,
				Poco::UInt16 nHttpPort,
				Poco::Net::HTTPServerParams::Ptr pServerParams);
		virtual ~httpServer();

		void configure(config::PluginConfig::pointer pPluginCfg);
		void initialise();

		void start();
		void stop();

		bool stoped();

	private:

		typedef Poco::ClassLoader<pocoHttpServer::requesthandler::IRequestHandler> RequestHandlerLoader;
		typedef Poco::Manifest<pocoHttpServer::requesthandler::IRequestHandler> RequestHandlerManifest;

		requesthandler::MyRequestHandlerFactory::Ptr _pRequestFactory;
		requesthandler::CRequestHandlerStore::Ptr _pRequestHandlerStore;
		Poco::Net::HTTPServer _srv; // HTTP server
		bool _stoped;
		
		requesthandler::IRequestHandler* loadRequestHandler(const std::string sLibraryFile,
					const std::string sUri);
	};
}

#endif /* HTTPSERVER_H */

