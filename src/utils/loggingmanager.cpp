/**
 * @file loggingmanager.cpp
 * Implementation file for class CLoggingManager
 *
 * @author João Silva <jazzy.blog@gmail.com>
 *
 * @date September 1, 2019
 *
 * @par Copyright:
 * 
 */

#include "loggingmanager.h"

#include "config/logconfig.h"

#include <Poco/AutoPtr.h>
#include <Poco/ConsoleChannel.h>
#include <Poco/FileChannel.h>
#include <Poco/AsyncChannel.h>
#include <Poco/PatternFormatter.h>
#include <Poco/FormattingChannel.h>
#include <Poco/LogStream.h>
#include <Poco/SplitterChannel.h>

using namespace pocoHttpServer::utils;

CLoggingManager::pointer CLoggingManager::_pManager(new CLoggingManager);

CLoggingManager::CLoggingManager()
{
}

CLoggingManager::CLoggingManager(const CLoggingManager& orig)
{
}

CLoggingManager::~CLoggingManager()
{
}

CLoggingManager::pointer CLoggingManager::getLoggingManager()
{
	return _pManager;
}

void CLoggingManager::configure(config::CLogConfig::pointer pLogConfig)
{
	// First of all, let's make some global configurations
	Poco::AutoPtr<Poco::Channel> pChannel;
	switch (pLogConfig->getLogType())
	{
		case config::CLogConfig::OT_Console:
			pChannel = Poco::AutoPtr<Poco::ConsoleChannel>(new Poco::ConsoleChannel);
			break;
		case config::CLogConfig::OT_File:
			pChannel = Poco::AutoPtr<Poco::FileChannel>(new Poco::FileChannel);
			if(pLogConfig->getFileLog())
			{
				pChannel->setProperty("path", pLogConfig->getFileLog()->getFilename());
				pChannel->setProperty("rotation", pLogConfig->getFileLog()->getRotation());
				pChannel->setProperty("archive", "timestamp");
				pChannel->setProperty("times", "utc");
				pChannel->setProperty("purgeCount", "10");
			}
			break;
	}
	//	Poco::AutoPtr<Poco::ConsoleChannel> pConsole(new Poco::ConsoleChannel);
	Poco::AutoPtr<Poco::PatternFormatter> pPF(new Poco::PatternFormatter);
	pPF->setProperty("pattern", "%v[10] | %q | %Y%m%d %H:%M:%S.%i | %t");
	pPF->setProperty("times", "local");
	Poco::AutoPtr<Poco::FormattingChannel> pFC(new Poco::FormattingChannel(pPF, pChannel));
	Poco::AutoPtr<Poco::AsyncChannel> pAsync(new Poco::AsyncChannel(pFC));

	Poco::Logger::root().setChannel(pAsync);

	// Parses log configuration
	config::CLogConfig::DebugLevelIter logLevelIter = pLogConfig->DebugLevelBegin();
	config::CLogConfig::DebugLevelIter endIter = pLogConfig->DebugLevelEnd();

	for (; logLevelIter != endIter; ++logLevelIter)
	{
		addModule(logLevelIter->first, logLevelIter->second);
	}
}

void CLoggingManager::addModule(std::string sModule, Poco::Message::Priority prio)
{
	Poco::Logger& logger = Poco::Logger::get(sModule);
	logger.setLevel(prio);
//	_mLoggers.insert(std::pair<std::string, Poco::Logger&>(sModule, logger));

	Poco::LogStream lstr(logger);
	//	
	//	lstr.fatal() << "Configured logging module "
	//			<< sModule
	//			<< " with priority "
	//			<< prio
	//			<< std::endl;
}

Poco::Logger& CLoggingManager::getLogger(std::string sModule)
{

//	if (_mLoggers.count(sModule))
//	{
//		return _mLoggers.at(sModule);
//	}

	Poco::Logger& logger = Poco::Logger::get(sModule);
//	_mLoggers.insert(std::pair<std::string, Poco::Logger&>(sModule, logger));

	return logger;
}
